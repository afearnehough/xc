#include "app_api.h"
#include "const.h"

//! Misc Functions

void* _memdupl(const void *value, int size_e) {
    //duplicate memory
    void *ptr = malloc(size_e);
    assert(ptr != NULL);
    memcpy(ptr,value,size_e);
    return ptr;
}

char* _strdupl(const char *value, int length) {
    //duplicate string
    char *str = malloc(length+1);
    assert(str != NULL);
    memcpy(str,value,length+1);
    return str;
}

//! List Functions

int _list_init(_list *list, int size_e) {
    assert(list->data == NULL);
    assert(size_e > 0);
    //allocate list data block
    void *data = malloc(LIST_CAPACITY*size_e);
    assert(data != NULL);
    //set default attributes
    list->data = data;
    list->size_e = size_e;
    list->capacity = LIST_CAPACITY;
    list->length = 0;
    return true;
}

int _list_reserve(_list *list, int capacity) {
    assert(list->data != NULL);
    //check current capacity
    if (capacity > list->capacity) {
        //increase capacity
        list->data = realloc(list->data,capacity*list->size_e);
        assert(list->data != NULL);
        list->capacity = capacity;
    } else {
        return false;
    }
    return true;
}

int _list_destroy(_list *list) {
    //clean-up list data
    free(list->data);
    list->data = NULL;
    return true;
}

int _list_insert(_list *list, const void *data) {
    assert(list->data != NULL);
    //check current capacity
    if ((list->length+1) >= list->capacity) {
        //increase list capacity
        _list_reserve(list,list->capacity*LIST_GROWTH);
    }
    //copy element to end of list
    memcpy(_list_data(list,list->length),data,list->size_e);
    list->length++;
    return true;
}

int _list_reverse(_list *list) {
    assert(list->data != NULL);
    //check current capacity
    if ((list->length+1) >= list->capacity) {
        //increase list capacity
        _list_reserve(list,list->capacity*LIST_GROWTH);
    }
    //reverse list by swaping elements
    void *temp = _list_data(list,list->length+1);
    for (int i=0; i<(list->length>>1); ++i) {
        void *cur1 = _list_data(list,i);
        void *cur2 = _list_data(list,list->length-(i+1));
        memcpy(temp,cur1,list->size_e);
        memcpy(cur1,cur2,list->size_e);
        memcpy(cur2,temp,list->size_e);
    }
    return true;
}

void* _list_data(_list *list ,int index) {
    assert(list->data != NULL);
    //return pointer to node at index
    return (void*)((char*)list->data+index*list->size_e);
}

void* _list_at(_list *list, int index) {
    if (index >= 0 && index < list->length) {
        return _list_data(list,index);
    }
    return NULL;
}

void* _list_back(_list *list, int index) {
    //return pointer to node at end of list
    return _list_at(list,list->length-index-1);
}

int _list_remove(_list *list, int index) {
    assert(list->data != NULL);
    assert(index < list->length);
    //shift all elements after deleted index back by one
    int new_ln = (list->length-1);
    if (index != new_ln && new_ln > 1) {
        void *dest = _list_data(list,index);
        void *tail = _list_data(list,index+1);
        memcpy(dest,tail,list->size_e*new_ln);
    }
    list->length = new_ln;
    return true;
}

int _list_wipe(_list *list) {
    assert(list->data != NULL);
    //wipe list memory
    memset(list->data,0,list->capacity);
    list->length = 0;
    return true;
}

int _list_is_empty(_list *list) {
    return (list->data == NULL || list->length <= 0);
}

//! Dictionary Functions

int _dict_init(_dict *dict) {
    _list_init(&dict->pair_list,sizeof(_pair));
    return true;
}

int _dict_destroy(_dict *dict) {
    //clean-up dictionary data
    for (int i=0; i<dict->pair_list.length; ++i) {
        _pair *pair = _list_at(&dict->pair_list,i);
        free(pair->kp);
        free(pair->vp);
    }
    _list_destroy(&dict->pair_list);
    return true;
}

int _dict_insert(_dict *dict, const char *name, const void *data, int size_e) {
    assert(size_e > 0);
    //check if pair is already defined
    if (_dict_at(dict,name) == NULL) {
        _pair pair;
        //allocate memory for key
        pair.size_k = strlen(name)+1;
        pair.kp = _strdupl(name,pair.size_k);
        //allocate memory for value
        pair.size_v = size_e;
        pair.vp = _memdupl(data,size_e);
        //insert pair object
        _list_insert(&dict->pair_list,&pair);
        return true;
    }
    return false;
}

void* _dict_at(_dict *dict, const char *name) {
    //find pair with specified name
    int index = _dict_index(dict,name);
    if (index >= 0) {
        _pair *pair = _list_at(&dict->pair_list,index);
        if (pair != NULL) {
            return pair->vp;
        }
    }
    return NULL;
}

void* _dict_at_index(_dict *dict, int index, int f_name) {
    //return pair value at specified index
    _pair *pair = _list_at(&dict->pair_list,index);
    if (pair != NULL) {
        return (f_name) ? pair->kp:pair->vp;
    }
    return NULL;
}

int _dict_index(_dict *dict, const char *name) {
    //find pair with specified name
    if (name != NULL) {
        for (int i=0; i<dict->pair_list.length; ++i) {
            _pair *pair = _list_at(&dict->pair_list,i);
            if (strcmp(name,pair->kp) == 0) {
                return i;
            }
        }
    }
    return -1;
}

int _dict_wipe(_dict *dict) {
    if (_list_wipe(&dict->pair_list)) {
        return true;
    }
    return false;
}

int _dict_is_empty(_dict *dict) {
    return (_list_is_empty(&dict->pair_list));
}

//! Flag Functions

int _flag_has(int flags, int flag) {
    if (flags & flag) {
        return true;
    }
    return false;
}

int _flag_set(int flags, int flag) {
    return (flags |= flag);
}

//! File Functions

int _file_init(_file *file, const char *mode) {
    //set default attributes
    file->file = NULL;
    file->scan_data = 0;
    file->scan_line = 0;
    //set file read/write mode
    if (strcmp(mode,"read") == 0) {
        file->mode = FILE_MODE_R;
    } else {
        file->mode = FILE_MODE_W;
    }
    return true;
}

int _file_destroy(_file *file) {
    if (_file_is_ready(file)) {
        fclose(file->file);
    }
    return true;
}

int _file_open(_file *file, const char *path) {
    if (!_file_is_ready(file)) {
        if (file->mode == FILE_MODE_R) {
            file->file = fopen(path,"r");
        } else {
            file->file = fopen(path,"w");
        }
        //check file pointer is valid
        if (!_file_is_ready(file)) {
            return false;
        }
        return true;
    }
    return false;
}

int _file_scan(_file *file) {
    if (_file_is_ready(file) && file->mode == FILE_MODE_R) {
        //read one character from file
        file->scan_data = fgetc(file->file);
        if (file->scan_data == '\n' ||
            file->scan_data == EOF) {
            file->scan_line++;
        }
        if (feof(file->file)) {
            return false;
        }
    } else {
        return false;
    }
    return true;
}

int _file_putf(_file *file, const char *text, ...) {
    if (_file_is_ready(file) && file->mode == FILE_MODE_W) {
        //write formatted string to file
        va_list args;
        va_start(args,text);
        vfprintf(file->file,text,args);
        va_end(args);
    } else {
        return false;
    }
    return true;
}

int _file_is_ready(_file *file) {
    //check file pointer is valid
    if (file->file == NULL) {
        return false;
    }
    return true;
}
