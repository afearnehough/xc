#include "app_api.h"

//! Core Functions

int _core_init(_core *core) {
    //initialise controller factory
    _dict_init(&core->ctrl_factory);
    //define controller factories
    _core_define(core,"c11",_controller_c11_factory);
    _core_define(core,"xml",_controller_xml_factory);
    return true;
}

int _core_destroy(_core *core) {
    //destroy controller factory
    _dict_destroy(&core->ctrl_factory);
    return true;
}

int _core_define(_core *core, const char *name, _controller_fn_factory fn) {
    //define new controller factory function
    _controller_factory factory = {fn};
    if (_dict_insert(&core->ctrl_factory,name,&factory,sizeof(_controller_factory))) {
        return true;
    }
    return false;
}

int _core_exec(_core *core, _dict *args) {
    //check output language specified (controller type)
    if (_dict_at(args,"l") != NULL) {
        _controller_factory *factory = _dict_at(&core->ctrl_factory,_dict_at(args,"l"));
        if (factory != NULL) {
            //create new controller object
            _controller *controller = factory->fn();
            if (controller != NULL) {
                //initialise and execute controller
                if (_controller_init(controller,args)) {
                    _controller_exec(controller,args);
                }
                //clean-up resources
                //_controller_destroy(controller);
                //free(controller);
            }
        } else {
            printf("Unsupported output language.");
        }
    } else {
        printf("No output language specified.");
    }
    return true;
}
