#include "app_api.h"
#include "const.h"

//! Token Functions

int _parser_token_init(_parser_token *token) {
    //initialise character list
    _list_init(&token->char_list,sizeof(char));
    //set default attributes
    token->t_index = -1;
    token->line = -1;
    return true;
}

int _parser_token_destroy(_parser_token *token) {
    //destroy character list
    _list_destroy(&token->char_list);
    return true;
}

int _parser_token_push_char(_parser_token *token, char character) {
    if (_list_insert(&token->char_list,&character)) {
        return true;
    }
    return false;
}

int _parser_token_reset(_parser_token *token) {
    //reset character list
    _list_wipe(&token->char_list);
    //reset attribute values
    token->t_index = -1;
    return true;
}

//! Lexer Functions

int _parser_lexer_init(_parser_lexer *lexer) {
    //initialise token list
    _list_init(&lexer->token_list,sizeof(_parser_token));
    //initialise token type dict
    _dict_init(&lexer->token_type_dict);
    //define special tokens
    _parser_lexer_define(lexer,"<number>");
    _parser_lexer_define(lexer,"<identity>");
    _parser_lexer_define(lexer,"<symbol>");
    _parser_lexer_define(lexer,"<comment>");
    _parser_lexer_define(lexer,"<string>");
    return true;
}

int _parser_lexer_destroy(_parser_lexer *lexer) {
    //destroy token list
    for (int i=0; i<lexer->token_list.length; ++i) {
        _parser_token_destroy(_list_at(&lexer->token_list,i));
    }
    _list_destroy(&lexer->token_list);
    //destroy token type dict
    _dict_destroy(&lexer->token_type_dict);
    return true;
}

int _parser_lexer_define(_parser_lexer *lexer, const char *code) {
    //check if token type already defined
    if (_dict_at(&lexer->token_type_dict,code) == NULL) {
        //insert new token type
        if (_dict_insert(&lexer->token_type_dict,code,code,strlen(code)+1)) {
            return true;
        }
    }
    return false;
}

int _parser_lexer_push(_parser_lexer *lexer, _parser_token *token) {
    int f_ln = token->char_list.length, f_offset = 0;
    //get reference to token type dictionary
    _dict *type_dict = &lexer->token_type_dict;
    //process characters
    while (f_offset < token->char_list.length) {
        int b_id = 0, b_ln = 0, c_ln = 0;
        //create new token object
        _parser_token copy;
        _parser_token_init(&copy);
        copy.t_index = token->t_index;
        copy.line = token->line;
        //find best token type match for all non number/string tokens
        if (copy.t_index != _dict_index(type_dict,"<number>") &&
            copy.t_index != _dict_index(type_dict,"<string>")) {
            //search for token type definition
            _list *pair_list = &lexer->token_type_dict.pair_list;
            for (int i=0; i<pair_list->length; ++i) {
                //compare characters
                const char *code = ((_pair*)_list_at(pair_list,i))->vp;
                for (int j=0; j<MIN(strlen(code),f_ln); ++j) {
                    if (code[j] != *(char*)_list_at(&token->char_list,f_offset+j)) {
                        break;
                    }
                    c_ln = (j+1);
                }
                if (c_ln > b_ln && c_ln == strlen(code)) {
                    if (token->t_index == _dict_index(type_dict,"<identity>")) {
                        //check comparasion length equals total token length
                        if (c_ln != f_ln) {
                            continue;
                        }
                    }
                    b_ln = c_ln;
                    b_id = i;
                }
                c_ln = 0;
            }
            //set token type for best match
            if (b_id > 0) {
                copy.t_index = b_id;
                if (token->t_index == _dict_index(type_dict,"<symbol>")) {
                    f_ln = b_ln;
                }
            }
        }
        //duplicate token characters if needed
        if (copy.t_index == _dict_index(type_dict,"<number>") ||
            copy.t_index == _dict_index(type_dict,"<identity>") ||
            copy.t_index == _dict_index(type_dict,"<string>")) {
            for (int c=0; c<token->char_list.length; ++c) {
                _list_insert(&copy.char_list,_list_at(&token->char_list,c));
            }
            _list_insert(&copy.char_list,&(char){0});
        } else {
            //clean-up unused resources
            _parser_token_destroy(&copy);
        }
        //put new token object
        _list_insert(&lexer->token_list,&copy);
        //set token fragment offset and length
        f_offset += f_ln;
        f_ln = (token->char_list.length-f_offset);
    }
    _parser_token_reset(token);
    return true;
}

int _parser_lexer_read(_parser_lexer *lexer, _parser_token *token, char character) {
    //check character is valid
    if (character > 0 || character == EOF) {
        //get base token type map indexes
        _dict *type_dict = &lexer->token_type_dict;
        int t_number = _dict_index(type_dict,"<number>");
        int t_identity = _dict_index(type_dict,"<identity>");
        int t_symbol = _dict_index(type_dict,"<symbol>");
        int t_comment = _dict_index(type_dict,"<comment>");
        int t_string = _dict_index(type_dict,"<string>");
        //handle token with invalid base index
        if (token->t_index < 0) {
            if (_parser_lexer_is_numeric(character)) {
                token->t_index = t_number;
            } else if (character == '"') {
                token->t_index = t_string;
                return true;
            } else if (_parser_lexer_is_alphanumeric(character)) {
                token->t_index = t_identity;
            } else if (_parser_lexer_is_symbol(character)) {
                token->t_index = t_symbol;
            } else if (character == '#') {
                token->t_index = t_comment;
            } else {
                return false;
            }
        }
        //handle token base index
        if (token->t_index == t_number) {
            if (_parser_lexer_is_numeric(character)) {
                _parser_token_push_char(token,character);
            } else {
                goto next_token;
            }
        } else if (token->t_index == t_string) {
            //check if closing string quote reached
            if (character != '"') {
                _parser_token_push_char(token,character);
            } else {
                //ensure empty strings are still inserted
                if (token->char_list.length == 0) {
                    _parser_token_push_char(token,' ');
                }
                character = 0;
                goto next_token;
            }
        } else if (token->t_index == t_identity) {
            if (_parser_lexer_is_alphanumeric(character)) {
                _parser_token_push_char(token,character);
            } else {
                goto next_token;
            }
        } else if (token->t_index == t_symbol) {
            if (_parser_lexer_is_symbol(character)) {
                _parser_token_push_char(token,character);
            } else {
                goto next_token;
            }
        } else if (token->t_index == t_comment) {
            //check if end of line reached
            if (character == '\n') {
                goto next_token;
            }
        }
        return true;
        next_token:
            //try to record current token
            _parser_lexer_push(lexer,token);
            //re-process input data
            _parser_lexer_read(lexer,token,character);
            return true;
    }
    return false;
}

int _parser_lexer_in_string(char character, const char *text) {
    return (strchr(text,character) == NULL) ? false : true;
}

int _parser_lexer_is_numeric(char character) {
    return _parser_lexer_in_string(character,"0123456789");
}

int _parser_lexer_is_alphanumeric(char character) {
    return _parser_lexer_in_string(character,"abcdefghijklomnpqrstuvwxyzABCDEFGHIJKLOMNPQRSTUVWXYZ0123456789_");
}

int _parser_lexer_is_symbol(char character) {
    return _parser_lexer_in_string(character,"@%-+*/=!?:;,.|&{}()[]<>~");
}

int _parser_lexer_exec(_parser_lexer *lexer, _file *file) {
    //check source file is loaded
    if (_file_is_ready(file)) {
        _parser_token token;
        _parser_token_init(&token);
        //process all characters in file
        while (_file_scan(file)) {
            //set token attributes
            token.line = file->scan_line;
            //process current character
            _parser_lexer_read(lexer,&token,file->scan_data);
        }
        //process end of file
        _parser_lexer_read(lexer,&token,EOF);
        _parser_token_destroy(&token);
    } else {
        return false;
    }
    return true;
}

int _parser_lexer_wipe(_parser_lexer *lexer) {
    //wipe token list
    for (int i=0; i<lexer->token_list.length; ++i) {
        _parser_token_destroy(_list_at(&lexer->token_list,i));
    }
    _list_wipe(&lexer->token_list);
    return true;
}

//! Node Functions

int _parser_node_init(_parser_node *node) {
    //initialise attr dictionary
    _dict_init(&node->attr_dict);
    //initialise node list
    _list_init(&node->nptr_list,sizeof(_parser_node*));
    //set default attributes
    node->parent  = NULL;
    node->r_index = -1;
    node->n_ident = -1;
    node->t_index =  0;
    node->t_start =  0;
    return true;
}

int _parser_node_destroy(_parser_node *node, int f_recursive) {
    //destroy attr dictionary
    _dict_destroy(&node->attr_dict);
    //destroy node list (if recursive mode enabled)
    if (f_recursive) {
        for (int i=0; i<node->nptr_list.length; ++i) {
            _parser_node_destroy(LIST_AT_PTR(_parser_node,&node->nptr_list,i),true);
        }
    }
    _list_destroy(&node->nptr_list);
    return true;
}

int _parser_node_push_attr(_parser_node *node, const char *name, const char *data) {
    if (_dict_insert(&node->attr_dict,name,data,strlen(data)+1)) {
        return true;
    }
    return false;
}

int _parser_node_push_node(_parser_node *node, _parser_node *data, int f_merge) {
    if (f_merge) {
        //insert child nodes and attributes into specified node
        for (int i=0; i<data->nptr_list.length; ++i) {
            _parser_node *curr = LIST_AT_PTR(_parser_node,&data->nptr_list,i);
            _parser_node_push_node(node,curr,false);
        }
        for (int i=0; i<data->attr_dict.pair_list.length; ++i) {
            const char *name = _dict_at_index(&data->attr_dict,i,true);
            const char *attr = _dict_at_index(&data->attr_dict,i,false);
            _parser_node_push_attr(node,name,attr);
        }
    } else {
        data->parent = node;
        if (!_list_insert(&node->nptr_list,&data)) {
            return false;
        }
    }
    return true;
}

//! Rule Functions

int _parser_rule_init(_parser_rule *rule) {
    //initialise statement list
    _list_init(&rule->stmt_list,sizeof(_parser_stmt));
    //initialise data data list
    _list_init(&rule->data_list,sizeof(_pair));
    //set default attributes
    rule->r_index = -1;
    rule->r_flags =  0;
    return true;
}

int _parser_rule_destroy(_parser_rule *rule) {
    //destroy statement list
    _list_destroy(&rule->stmt_list);
    //destroy data data list
    _parser_rule_data_wipe(rule);
    _list_destroy(&rule->data_list);
    return true;
}

int _parser_rule_push_stmt(_parser_rule *rule, _parser_stmt *stmt) {
    if (_list_insert(&rule->stmt_list,stmt)) {
        return true;
    }
    return false;
}

int _parser_rule_data_wipe(_parser_rule *rule) {
    if (_list_wipe(&rule->data_list)) {
        return true;
    }
    return true;
}

_pair* _parser_rule_data_find(_parser_rule *rule, int t_start) {
    //try to find and return node reference with specified token start index
    for (int i=0; i<rule->data_list.length; ++i) {
        _pair *data = _list_at(&rule->data_list,i);
        //check current data element
        _parser_node *node = data->kp;
        if (node->t_start == t_start) {
            return data;
        }
    }
    return NULL;
}

int _parser_rule_push_data(_parser_rule *rule, _pair *data) {
    if (_list_insert(&rule->data_list,data)) {
        return true;
    }
    return false;
}

//! Parser Functions

int _parser_init(_parser *parser) {
    //initialise rule dict
    _dict_init(&parser->rule_dict);
    //initialise token lexer
    _parser_lexer_init(&parser->lexer);
    //initialise node tree
    _parser_node_init(&parser->tree);
    //set default attributes
    parser->n_count = 0;
    return true;
}

int _parser_destroy(_parser *parser) {
    //destroy rule dict
    _list *pair_list = &parser->rule_dict.pair_list;
    for (int i=0; i<pair_list->length; ++i) {
        _parser_rule_destroy(_dict_at_index(&parser->rule_dict,i,false));
    }
    _dict_destroy(&parser->rule_dict);
    //destroy token lexer
    _parser_lexer_destroy(&parser->lexer);
    //destroy node tree
    _parser_node_destroy(&parser->tree,true);
    return true;
}

int _parser_rule_define(_parser *parser, const char *name, int flags) {
    //check if rule not already defined
    if (_dict_at(&parser->rule_dict,name) == NULL) {
        //create empty rule object
        _parser_rule rule;
        _parser_rule_init(&rule);
        //insert empty rule object
        if (_dict_insert(&parser->rule_dict,name,&rule,sizeof(_parser_rule))) {
            //find index of new rule and set rule t_index attribute
            _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
            rule_ptr->r_index = _dict_index(&parser->rule_dict,name);
            rule_ptr->r_flags = flags;
            return true;
        }
    }
    return false;
}

int _parser_rule_branch(_parser *parser, const char *name, const char *rule) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add statement to specified rule
        int value = _dict_index(&parser->rule_dict,rule);
        int flags = _flag_set(0,STMT_BRANCH);
        if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                NULL,NULL,value,flags
            })) {
            return true;
        }
    }
    return false;
}

int _parser_rule_expect(_parser *parser, const char *name, const char *code) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add statement to specified rule
        int value = _dict_index(&parser->lexer.token_type_dict,code);
        if (value < 0) {
            //define new token type if not defined
            if (_parser_lexer_define(&parser->lexer,code)) {
                value = _dict_index(&parser->lexer.token_type_dict,code);
            }
        }
        int flags = _flag_set(0,STMT_EXPECT);
        if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                NULL,NULL,value,flags
            })) {
            return true;
        }
    }
    return false;
}

int _parser_rule_attrib(_parser *parser, const char *name, const char *attr) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add attribute to the lastest statement
        //NOTE: lastest statement must be an expect statement
        _parser_stmt *stmt_ptr = _list_back(&rule_ptr->stmt_list,0);
        if (stmt_ptr != NULL) {
            if (_flag_has(stmt_ptr->s_flags,STMT_EXPECT)) {
                stmt_ptr->s_flags = _flag_set(stmt_ptr->s_flags,STMT_ATTRIB);
                stmt_ptr->s_attr = _strdupl(attr,strlen(attr)+1);
                return true;
            }
        }
    }
    return false;
}

int _parser_rule_mfname(_parser *parser, const char *name, const char *attr) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add statement to specified rule
        int flags = _flag_set(0,STMT_METAFL);
        if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                _strdupl(attr,strlen(attr)+1),NULL,0,flags
            })) {
            return true;
        }
    }
    return false;

}
int _parser_rule_mfdata(_parser *parser, const char *name, const char *data) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add data for latest metafield
        _parser_stmt *stmt_ptr = _list_back(&rule_ptr->stmt_list,0);
        if (stmt_ptr != NULL) {
            if (_flag_has(stmt_ptr->s_flags,STMT_METAFL)) {
                if (stmt_ptr->s_data == NULL) {
                    stmt_ptr->s_data = _strdupl(data,strlen(data)+1);
                    return true;
                }
            }
        }
    }
    return false;
}

int _parser_rule_option(_parser *parser, const char *name, const char *rule) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add statement to specified rule
        int value = _dict_index(&parser->rule_dict,rule);
        int flags = _flag_set(0,STMT_BRANCH|STMT_OPTION);
        if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                NULL,NULL,value,flags
            })) {
            return true;
        }
    }
    return false;
}

int _parser_rule_repeat(_parser *parser, const char *name, const char *rule) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add statement to specified rule
        int value = _dict_index(&parser->rule_dict,rule);
        int flags = _flag_set(0,STMT_BRANCH|STMT_REPEAT|STMT_OPTION);
        if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                NULL,NULL,value,flags
            })) {
            return true;
        }
    }
    return false;
}

int _parser_rule_altern(_parser *parser, const char *name, const char *rule) {
    _parser_rule *rule_ptr = _dict_at(&parser->rule_dict,name);
    if (rule_ptr != NULL) {
        //add alternative for latest statement
        _parser_stmt *stmt_ptr = _list_back(&rule_ptr->stmt_list,0);
        if (stmt_ptr != NULL) {
            //add alternative for branch statements
            if (_flag_has(stmt_ptr->s_flags,STMT_BRANCH)) {
                int value = _dict_index(&parser->rule_dict,rule);
                int flags = _flag_set(0,STMT_BRANCH|STMT_ALTERN);
                if (_parser_rule_push_stmt(rule_ptr,&(_parser_stmt) {
                        NULL,NULL,value,flags
                    })) {
                    return true;
                }
            }
        }
    }
    return false;
}

int _parser_read(_parser *parser, _parser_rule *rule) {
    int r_state = true;
    int t_index = 0;
    int t_error = 0;
    int s_repst = true;
    //create and initialise parser stack
    _list stack;
    _list_init(&stack,sizeof(_parser_frame));
    _list_insert(&stack,&(_parser_frame) {
        &parser->tree,NULL,rule->r_index,0,0
    });
    //parse all tokens until stack is depleted
    while (stack.length) {
        //pop current rule stack frame
        _parser_frame frame = *(_parser_frame*)_list_back(&stack,0);
        _list_remove(&stack,stack.length-1);
        //get reference to current rule
        _parser_rule *rule_ptr = _dict_at_index(&parser->rule_dict,frame.r_index,false);
        //check if branch rule is already datad for current token index
        _pair *data_ptr = _parser_rule_data_find(rule_ptr,t_index);
        if (data_ptr != NULL && frame.prev != NULL) {
            //push or merge current completed rule into rule data data
            _parser_node_push_node(frame.prev,data_ptr->kp,
                _flag_has(rule_ptr->r_flags,RULE_PASSIVE));
            //set new token index position and move back to parent rule
            t_index = ((_parser_node*)data_ptr->kp)->t_index;
            continue;
        }
        //process all statements
        for (; frame.s_index<rule_ptr->stmt_list.length; ++frame.s_index) {
            _parser_stmt *stmt_ptr = _list_at(&rule_ptr->stmt_list,frame.s_index);
            //process statement if rule state true or is alternative statement
            if (r_state != _flag_has(stmt_ptr->s_flags,STMT_ALTERN)) {
                r_state = true;
                //check if current statement is a branch
                if (_flag_has(stmt_ptr->s_flags,STMT_BRANCH)) {
                    //offset statement index if not repeating
                    int s_idxos = 1;
                    if (_flag_has(stmt_ptr->s_flags,STMT_REPEAT)) {
                        if (s_repst) {
                            s_idxos = 0;
                        } else {
                            s_repst = true;
                            continue;
                        }
                    }
                    //create and initialise new parser node
                    _parser_node *node = malloc(sizeof(_parser_node));
                    assert(node != NULL);
                    _parser_node_init(node);
                    node->r_index = stmt_ptr->s_value;
                    node->n_ident = parser->n_count++;
                    node->t_index = 0;
                    node->t_start = t_index;
                    //push current frame
                    _list_insert(&stack,&(_parser_frame) {
                        frame.curr,frame.prev,frame.r_index,frame.s_index+s_idxos,frame.o_flags
                    });
                    //push frame for next iteration
                    _list_insert(&stack,&(_parser_frame) {
                        node,frame.curr,stmt_ptr->s_value,0,stmt_ptr->s_flags
                    });
                    break;
                } else if (_flag_has(stmt_ptr->s_flags,STMT_EXPECT)) {
                    //get reference to current token
                    _parser_token *token = _list_at(&parser->lexer.token_list,t_index);
                    //check if specified token equals token at current index
                    if (token == NULL || token->t_index != stmt_ptr->s_value) {
                        if (!_flag_has(stmt_ptr->s_flags,STMT_OPTION)) {
                            r_state = false;
                            if (t_index > t_error) {
                                //set new token error index
                                t_error = t_index;
                            }
                        }
                        t_index = frame.curr->t_start;
                        break;
                    } else {
                        //check for attribute for current node
                        if (_flag_has(stmt_ptr->s_flags,STMT_ATTRIB)) {
                            if (_list_is_empty(&token->char_list)) {
                                _parser_node_push_attr(frame.curr,stmt_ptr->s_attr,stmt_ptr->s_attr);
                            } else {
                                const char *data = _list_data(&token->char_list,0);
                                _parser_node_push_attr(frame.curr,stmt_ptr->s_attr,data);
                            }
                        }
                        t_index++;
                    }
                } else if (_flag_has(stmt_ptr->s_flags,STMT_METAFL)) {
                    //insert new attribute for current node
                    _parser_node_push_attr(frame.curr,stmt_ptr->s_attr,stmt_ptr->s_data);
                }
            }
        }
        //check if current rule was successful
        if (r_state) {
            //check if entire rule was processed
            if (frame.s_index >= rule_ptr->stmt_list.length) {
                //check if previous node valid and current node not empty
                if (frame.prev != NULL &&
                    (_flag_has(rule_ptr->r_flags,RULE_STICK) ||
                    !_list_is_empty(&frame.curr->nptr_list) ||
                    !_dict_is_empty(&frame.curr->attr_dict))) {
                    //push or merge current completed rule into rule data data
                    frame.curr->t_index = t_index;
                    _parser_node_push_node(frame.prev,frame.curr,
                        _flag_has(rule_ptr->r_flags,RULE_PASSIVE));
                    //push current completed rule into rule data data
                    if (_flag_has(rule_ptr->r_flags,RULE_CACHED)) {
                        _parser_rule_push_data(rule_ptr,&(_pair) {
                            .kp = frame.curr, .vv = 0
                        });
                    }
                }
            }
        } else if (_flag_has(frame.o_flags,STMT_OPTION)) {
            //disable repetition if origin statement if flagged as repeat
            s_repst = !_flag_has(frame.o_flags,STMT_REPEAT);
            r_state = true;
        } else {
            //destroy redundant node
            _parser_node_destroy(frame.curr,false);
            free(frame.curr);
        }
    }
    //clean-up stack
    _list_destroy(&stack);
    //check for unexpected token error
    if (t_error >= t_index) {
        _parser_token *token = _list_at(&parser->lexer.token_list,t_error);
        if (token != NULL) {
            printf("Error: Line %i: Unexpected token: '%s'\n",token->line,
                _dict_at_index(&parser->lexer.token_type_dict,token->t_index,true));
            return false;
        }
    }
    return true;
}

_parser_node* _parser_node_malloc(_parser *parser, const char *type) {
    //create and initialise new parser node object
    _parser_node *node = malloc(sizeof(_parser_node));
    assert(node != NULL);
    _parser_node_init(node);
    //set node attributes
    node->r_index = _dict_index(&parser->rule_dict,type);
    node->n_ident = parser->n_count++;
    return node;
}

_parser_node* _parser_node_search_type(_parser *parser, _parser_node *node, const char *type) {
    //try to find and return node with specified rule type
    if (node != NULL) {
        for (int i=0; i<node->nptr_list.length; ++i) {
            _parser_node *curr = LIST_AT_PTR(_parser_node,&node->nptr_list,i);
            if (_parser_is_type(parser,curr,type)) {
                return curr;
            }
        }
    }
    return NULL;
}

_parser_node* _parser_node_search_attr(_parser *parser, _parser_node *node, const char *attr, const char *data) {
    //try to find and return node with specified attribute and data
    if (node != NULL) {
        for (int i=0; i<node->nptr_list.length; ++i) {
            _parser_node *curr = LIST_AT_PTR(_parser_node,&node->nptr_list,i);
            const char *a_data = _dict_at(&curr->attr_dict,attr);
            if (a_data != NULL && strcmp(a_data,data) == 0) {
                return curr;
            }
        }
    }
    return NULL;
}

int _parser_is_type(_parser *parser, _parser_node *node, const char *type) {
    //check if specified node is linked to specified rule type
    if (node != NULL) {
        const char *name = _dict_at_index(&parser->rule_dict,node->r_index,true);
        if (strcmp(name,type) == 0) {
            return true;
        }
    }
    return false;
}

int _parser_is_attr(_parser *parser, _parser_node *node, const char *attr) {
    //check if specified node has the specified attribute
    if (node != NULL) {
        if (_dict_at(&node->attr_dict,attr) != NULL) {
            return true;
        }
    }
    return false;
}

int _parser_exec(_parser *parser, const char *path) {
    //open and process source file
    //TODO: search library directories if initial file load fails
    _file file;
    _file_init(&file,"read");
    if (_file_open(&file,path)) {
        printf("Reading: %s\n",path);
        //perform source tokenisation
        _parser_lexer_exec(&parser->lexer,&file);
        //perform rule parsing
        _parser_rule *root = _dict_at(&parser->rule_dict,"root");
        if (root != NULL) {
            parser->tree.r_index = root->r_index;
            //wipe all rule node reference cache
            for (int i=0; i<parser->rule_dict.pair_list.length; ++i) {
                _parser_rule *rule_ptr = _dict_at_index(&parser->rule_dict,i,false);
                _parser_rule_data_wipe(rule_ptr);
            }
            //read and process code tokens
            int status = _parser_read(parser,root);
            _file_destroy(&file);
            _parser_lexer_wipe(&parser->lexer);
            return status;
        } else {
            printf("Error: Root rule not specified\n");
        }
    } else {
        printf("Error: File not found: %s\n",path);
    }
    _file_destroy(&file);
    return false;
}
