#include "app_api.h"

int main(int argc, char *argv[]) {
    //push arguments into dictionary
    _dict args;
    _dict_init(&args);
    for (int i=1; i<argc-1; ++i) {
        const char *data = argv[i+1];
        //argument: input file
        if (strcmp(argv[i],"-i") == 0) {
            _dict_insert(&args,"i",data,strlen(data)+1);
        }
        //argument: language controller
        if (strcmp(argv[i],"-l") == 0) {
            _dict_insert(&args,"l",data,strlen(data)+1);
        }
        //argument: output file
        if (strcmp(argv[i],"-o") == 0) {
            _dict_insert(&args,"o",data,strlen(data)+1);
        }
    }
    //create and initiate core
    _core core;
    if (_core_init(&core)) {
        _core_exec(&core,&args);
    }
    _core_destroy(&core);
    _dict_destroy(&args);
    return EXIT_SUCCESS;
}
