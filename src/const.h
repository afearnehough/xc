#ifndef CONST_H
#define CONST_H

#define LIST_GROWTH                 2
#define LIST_CAPACITY               1
#define FILE_MODE_R                 0x01
#define FILE_MODE_W                 0x02

#define MIN(a,b)                    ((a<b)?a:b)
#define MAX(a,b)                    ((a>b)?a:b)

#define STMT_EXPECT                 0x01
#define STMT_BRANCH                 0x02
#define STMT_REPEAT                 0x04
#define STMT_OPTION                 0x08
#define STMT_ATTRIB                 0x10
#define STMT_ALTERN                 0x20
#define STMT_METAFL                 0x40

#define RULE_PASSIVE                0x01
#define RULE_STICK                  0x02
#define RULE_CACHED                 0x04

#define NODE_EXPLORE_DEEPER         0x00
#define NODE_EXPLORE_IGNORE         0x01
#define NODE_PROCESS_HEAD           0x00
#define NODE_PROCESS_MIDDLE         0x01
#define NODE_PROCESS_LAST           0x02

#endif //CONST_H
