#include "app_api.h"
#include "const.h"

//! C11 Code Templates

#define C11_TPL_CREATE_HEADER "A"

//! C11 Controller Functions

int _controller_c11_init(_controller *controller) {
    return true;
}

_controller* _controller_c11_factory(void) {
    //create new c11 controller object
    _controller *controller = _controller_malloc();
    //attach controller functions
    controller->init = _controller_c11_init;
    controller->factory = _controller_c11_factory;
    controller->grammar = _controller_c11_grammar;
    controller->destroy = _controller_c11_destroy;
    controller->proc = _controller_c11_proc;
    return controller;
}

int _controller_c11_destroy(_controller *controller) {
    return true;
}

int _controller_c11_grammar(_controller *controller) {
    _parser *parser = &controller->parser;
    //add notifications for binary operators
    _parser_rule_mfname(parser,"op_mul","c11_op");
    _parser_rule_mfdata(parser,"op_mul","b");
    _parser_rule_mfname(parser,"op_mul","c11_op_sym");
    _parser_rule_mfdata(parser,"op_mul","*");
    _parser_rule_mfname(parser,"op_div","c11_op");
    _parser_rule_mfdata(parser,"op_div","b");
    _parser_rule_mfname(parser,"op_div","c11_op_sym");
    _parser_rule_mfdata(parser,"op_div","/");
    _parser_rule_mfname(parser,"op_mod","c11_op");
    _parser_rule_mfdata(parser,"op_mod","b");
    _parser_rule_mfname(parser,"op_mod","c11_op_sym");
    _parser_rule_mfdata(parser,"op_mod","%");
    _parser_rule_mfname(parser,"op_add","c11_op");
    _parser_rule_mfdata(parser,"op_add","b");
    _parser_rule_mfname(parser,"op_add","c11_op_sym");
    _parser_rule_mfdata(parser,"op_add","+");
    _parser_rule_mfname(parser,"op_sub","c11_op");
    _parser_rule_mfdata(parser,"op_sub","b");
    _parser_rule_mfname(parser,"op_sub","c11_op_sym");
    _parser_rule_mfdata(parser,"op_sub","-");
    _parser_rule_mfname(parser,"op_gt","c11_op");
    _parser_rule_mfdata(parser,"op_gt","b");
    _parser_rule_mfname(parser,"op_gt","c11_op_sym");
    _parser_rule_mfdata(parser,"op_gt",">");
    _parser_rule_mfname(parser,"op_gte","c11_op");
    _parser_rule_mfdata(parser,"op_gte","b");
    _parser_rule_mfname(parser,"op_gte","c11_op_sym");
    _parser_rule_mfdata(parser,"op_gte",">=");
    _parser_rule_mfname(parser,"op_lt","c11_op");
    _parser_rule_mfdata(parser,"op_lt","b");
    _parser_rule_mfname(parser,"op_lt","c11_op_sym");
    _parser_rule_mfdata(parser,"op_lt","<");
    _parser_rule_mfname(parser,"op_lte","c11_op");
    _parser_rule_mfdata(parser,"op_lte","b");
    _parser_rule_mfname(parser,"op_lte","c11_op_sym");
    _parser_rule_mfdata(parser,"op_lte","<=");
    _parser_rule_mfname(parser,"op_equal","c11_op");
    _parser_rule_mfdata(parser,"op_equal","b");
    _parser_rule_mfname(parser,"op_equal","c11_op_sym");
    _parser_rule_mfdata(parser,"op_equal","==");
    _parser_rule_mfname(parser,"op_not_equal","c11_op");
    _parser_rule_mfdata(parser,"op_not_equal","b");
    _parser_rule_mfname(parser,"op_not_equal","c11_op_sym");
    _parser_rule_mfdata(parser,"op_not_equal","!=");
    _parser_rule_mfname(parser,"op_and","c11_op");
    _parser_rule_mfdata(parser,"op_and","b");
    _parser_rule_mfname(parser,"op_and","c11_op_sym");
    _parser_rule_mfdata(parser,"op_and","&&");
    _parser_rule_mfname(parser,"op_or","c11_op");
    _parser_rule_mfdata(parser,"op_or","b");
    _parser_rule_mfname(parser,"op_or","c11_op_sym");
    _parser_rule_mfdata(parser,"op_or","||");
    _parser_rule_mfname(parser,"op_assign","c11_op");
    _parser_rule_mfdata(parser,"op_assign","b");
    _parser_rule_mfname(parser,"op_assign","c11_op_sym");
    _parser_rule_mfdata(parser,"op_assign","=");
    //add notifications for unary operators
    _parser_rule_mfname(parser,"op_inc","c11_op");
    _parser_rule_mfdata(parser,"op_inc","u");
    _parser_rule_mfname(parser,"op_inc","c11_op_sym");
    _parser_rule_mfdata(parser,"op_inc","++");
    _parser_rule_mfname(parser,"op_inc","c11_op_fix");
    _parser_rule_mfdata(parser,"op_inc","s");
    _parser_rule_mfname(parser,"op_dec","c11_op");
    _parser_rule_mfdata(parser,"op_dec","u");
    _parser_rule_mfname(parser,"op_dec","c11_op_sym");
    _parser_rule_mfdata(parser,"op_dec","--");
    _parser_rule_mfname(parser,"op_dec","c11_op_fix");
    _parser_rule_mfdata(parser,"op_dec","s");
    _parser_rule_mfname(parser,"op_pos","c11_op");
    _parser_rule_mfdata(parser,"op_pos","u");
    _parser_rule_mfname(parser,"op_pos","c11_op_sym");
    _parser_rule_mfdata(parser,"op_pos","+");
    _parser_rule_mfname(parser,"op_pos","c11_op_fix");
    _parser_rule_mfdata(parser,"op_pos","p");
    _parser_rule_mfname(parser,"op_neg","c11_op");
    _parser_rule_mfdata(parser,"op_neg","u");
    _parser_rule_mfname(parser,"op_neg","c11_op_sym");
    _parser_rule_mfdata(parser,"op_neg","-");
    _parser_rule_mfname(parser,"op_neg","c11_op_fix");
    _parser_rule_mfdata(parser,"op_neg","p");
    _parser_rule_mfname(parser,"op_not","c11_op");
    _parser_rule_mfdata(parser,"op_not","u");
    _parser_rule_mfname(parser,"op_not","c11_op_sym");
    _parser_rule_mfdata(parser,"op_not","!");
    _parser_rule_mfname(parser,"op_not","c11_op_fix");
    _parser_rule_mfdata(parser,"op_not","p");
    return true;
}

int _controller_c11_proc_deftab(_controller *controller, _list *path, int face) {
    //insert tab character for every scope node in path
    for (int i=0; i<path->length-1; ++i) {
        //fetch required node references
        _parser_node *curr = ((_pair*)_list_at(path,i))->kp;
        assert(v_node != NULL);
        if (_parser_is_type(&controller->parser,curr,"scope")) {
            _file_putf(&controller->writer,"\t");
        }
    }
}

int _controller_c11_proc_deftyp(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *v_node = ((_pair*)_list_back(path,0))->kp;
    assert(v_node != NULL);
    //output c data type for specified variable node
    if ((v_node = _parser_node_search_type(&controller->parser,v_node,"data_type")) != NULL) {
        const char *v_type = _dict_at(&v_node->attr_dict,"type");
        if (strcmp(v_type,"i08") == 0) {
            _file_putf(&controller->writer,"int8_t");
        } else if (strcmp(v_type,"u08") == 0) {
            _file_putf(&controller->writer,"uint8_t");
        } else if (strcmp(v_type,"i32") == 0) {
            _file_putf(&controller->writer,"int32_t");
        } else if (strcmp(v_type,"u32") == 0) {
            _file_putf(&controller->writer,"uint32_t");
        } else if (strcmp(v_type,"i64") == 0) {
            _file_putf(&controller->writer,"int64_t");
        } else if (strcmp(v_type,"u64") == 0) {
            _file_putf(&controller->writer,"uint64_t");
        } else if (strcmp(v_type,"f32") == 0) {
            _file_putf(&controller->writer,"float");
        } else if (strcmp(v_type,"f64") == 0) {
            _file_putf(&controller->writer,"double");
        } else {
            _file_putf(&controller->writer,"struct %s*",v_type);
        }
    } else {
        _file_putf(&controller->writer,"void");
    }
    return true;
}

int _controller_c11_proc_defvar(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *v_node = ((_pair*)_list_back(path,0))->kp;
    assert(v_node != NULL);
    //fetch variable attributes
    const char *v_name = _dict_at(&v_node->attr_dict,"name");
    //write variable declaration
    _controller_process(controller,v_node,_controller_c11_proc_deftyp);
    _file_putf(&controller->writer," %s",v_name);
    return true;
}

int _controller_c11_proc_defref(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *r_node = ((_pair*)_list_back(path,0))->kp;
    assert(r_node != NULL);
    //fetch reference attributes
    const char *r_name = _dict_at(&r_node->attr_dict,"name");
    //check if specified reference is a class member
    for (int i=0; i<path->length; ++i) {
        _parser_node *c_node = ((_pair*)_list_at(path,i))->kp;
        if (_parser_is_type(&controller->parser,c_node,"class")) {
            for (int j=0; j<c_node->nptr_list.length; ++j) {
                _parser_node *v_node = LIST_AT_PTR(_parser_node,&c_node->nptr_list,j);
                if (_parser_is_type(&controller->parser,v_node,"variable")) {
                    //fetch variable attributes
                    const char *v_name = _dict_at(&v_node->attr_dict,"name");
                    if (strcmp(r_name,v_name) == 0) {
                        _file_putf(&controller->writer,"self->%s",r_name);
                        return true;
                    }
                }
            }
        }
    }
    _file_putf(&controller->writer,"%s",r_name);
    return true;
}

int _controller_c11_proc_defopi(_controller *controller, _list *path, int face) {
    return true;
}

int _controller_c11_proc_defopa(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *o_node = ((_pair*)_list_back(path,0))->kp;
    assert(o_node != NULL);
    assert(o_node->nptr_list.length == 2);
    _parser_node *l_oper = LIST_AT_PTR(_parser_node,&o_node->nptr_list,0);
    _parser_node *r_oper = LIST_AT_PTR(_parser_node,&o_node->nptr_list,1);
    _controller_process(controller,l_oper,_controller_c11_proc_defref);
    return true;
}

int _controller_c11_proc_defexp(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *e_node = ((_pair*)_list_back(path,0))->kp;
    assert(e_node != NULL);
    //process and write current node
    //handle node: scope
    if (_parser_is_type(&controller->parser,e_node,"scope")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _file_putf(&controller->writer," {\n");
                break;
            case NODE_PROCESS_LAST:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _file_putf(&controller->writer,"}\n");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: variable
    if (_parser_is_type(&controller->parser,e_node,"variable")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _controller_process(controller,e_node,_controller_c11_proc_defvar);
                break;
            case NODE_PROCESS_LAST:
                _file_putf(&controller->writer,";\n");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: variable initialise
    if (_parser_is_type(&controller->parser,e_node,"variable_init")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _file_putf(&controller->writer," = ");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: if
    if (_parser_is_type(&controller->parser,e_node,"if")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _file_putf(&controller->writer,"if ");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: else if
    if (_parser_is_type(&controller->parser,e_node,"elseif")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _file_putf(&controller->writer,"else if ");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: else
    if (_parser_is_type(&controller->parser,e_node,"else")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _file_putf(&controller->writer,"else");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: while
    if (_parser_is_type(&controller->parser,e_node,"while")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                _file_putf(&controller->writer,"while ");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: condition | sub expression
    if (_parser_is_type(&controller->parser,e_node,"condition") ||
        _parser_is_type(&controller->parser,e_node,"sub")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _file_putf(&controller->writer,"(");
                break;
            case NODE_PROCESS_LAST:
                _file_putf(&controller->writer,")");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: statement
    if (_parser_is_type(&controller->parser,e_node,"stmt")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_deftab);
                break;
            case NODE_PROCESS_LAST:
                _file_putf(&controller->writer,";\n");
                break;
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: normal operators
    if (_parser_is_attr(&controller->parser,e_node,"c11_op")) {
        const char *sym = _dict_at(&e_node->attr_dict,"c11_op_sym");
        const char *ops = _dict_at(&e_node->attr_dict,"c11_op");
        //check number of operands
        if (ops[0] == 'u') {
            const char *fix = _dict_at(&e_node->attr_dict,"c11_op_fix");
            if ((fix[0] == 's' && face == NODE_PROCESS_LAST) ||
                (fix[0] == 'p' && face == NODE_PROCESS_HEAD)) {
                _file_putf(&controller->writer,"%s",sym);
            }
        } else if (ops[0] == 'b') {
            if (face == NODE_PROCESS_MIDDLE) {
                _file_putf(&controller->writer," %s ",sym);
            }
        }
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: op_access
    if (_parser_is_type(&controller->parser,e_node,"op_invoke")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_defopi);
                break;
        }
        return NODE_EXPLORE_IGNORE;
    }
    //handle node: op_access
    if (_parser_is_type(&controller->parser,e_node,"op_access")) {
        switch (face) {
            case NODE_PROCESS_HEAD:
                _controller_process(controller,e_node,_controller_c11_proc_defopa);
                break;
        }
        return NODE_EXPLORE_IGNORE;
    }
    //handle node: string
    if (_parser_is_type(&controller->parser,e_node,"str")) {
        _file_putf(&controller->writer,"\"",_dict_at(&e_node->attr_dict,"value"));
        _file_putf(&controller->writer,"%s",_dict_at(&e_node->attr_dict,"value"));
        _file_putf(&controller->writer,"\"",_dict_at(&e_node->attr_dict,"value"));
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: reference
    if (_parser_is_type(&controller->parser,e_node,"ref")) {
        _controller_process(controller,e_node,_controller_c11_proc_defref);
        return NODE_EXPLORE_DEEPER;
    }
    //handle node: value
    if (_parser_is_type(&controller->parser,e_node,"val")) {
        _file_putf(&controller->writer,"%s",_dict_at(&e_node->attr_dict,"value"));
        return NODE_EXPLORE_DEEPER;
    }
    return NODE_EXPLORE_DEEPER;
}

int _controller_c11_proc_deffpl(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *p_list = ((_pair*)_list_back(path,0))->kp;
    assert(p_list != NULL);
    for (int p=0; p<p_list->nptr_list.length; ++p) {
        _parser_node *p_node = LIST_AT_PTR(_parser_node,&p_list->nptr_list,p);
        //fetch parameter attributes
        const char *p_name = _dict_at(&p_node->attr_dict,"name");
        //write parameter
        _file_putf(&controller->writer,", ");
        _controller_process(controller,p_node,_controller_c11_proc_deftyp);
        _file_putf(&controller->writer," %s",p_name);
    }
}

int _controller_c11_proc_deffun(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *c_node = ((_pair*)_list_back(path,1))->kp;
    _parser_node *f_node = ((_pair*)_list_back(path,0))->kp;
    assert(c_node != NULL && f_node != NULL);
    //fetch function attributes
    const char *c_name = _dict_at(&c_node->attr_dict,"name");
    const char *f_name = _dict_at(&f_node->attr_dict,"name");
    //write function name and return type
    _controller_process(controller,f_node,_controller_c11_proc_deftyp);
    _file_putf(&controller->writer," %s_fn_%s",c_name,f_name);
    //write parameter list
    _file_putf(&controller->writer,"(struct %s* self",c_name);
    _parser_node *p_list = _parser_node_search_type(&controller->parser,f_node,"parameter_list");
    if (p_list != NULL) {
        _controller_process(controller,p_list,_controller_c11_proc_deffpl);
    }
    _file_putf(&controller->writer,")");
    return true;
}

int _controller_c11_proc_deffgc(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *c_node = ((_pair*)_list_back(path,1))->kp;
    _parser_node *f_node = ((_pair*)_list_back(path,0))->kp;
    assert(c_node != NULL && f_node != NULL);
    return true;
}

int _controller_c11_proc_header(_controller *controller) {
    _parser_node *root = &controller->parser.tree;
    for (int i=0; i<root->nptr_list.length; ++i) {
        _parser_node *c_node = LIST_AT_PTR(_parser_node,&root->nptr_list,i);
        if (_parser_is_type(&controller->parser,c_node,"class")) {
            //fetch class attributes
            const char *c_name = _dict_at(&c_node->attr_dict,"name");
            //write class structure header
            _file_putf(&controller->writer,"\nstruct %s {\n",c_name);
            //write class structure definitions
            for (int j=0; j<c_node->nptr_list.length; ++j) {
                _parser_node *v_node = LIST_AT_PTR(_parser_node,&c_node->nptr_list,j);
                //process variable nodes
                if (_parser_is_type(&controller->parser,v_node,"variable")) {
                    _file_putf(&controller->writer,"\t");
                    _controller_process(controller,v_node,_controller_c11_proc_defvar);
                    _file_putf(&controller->writer,";\n");
                }
            }
            //write class structure footer
            _file_putf(&controller->writer,"\tint32_t ref_count;\n};\n");
            //write class special prototype functions
            //_file_putf(&controller->writer,"struct %s* %s_create();\n",c_name,c_name);
            //_file_putf(&controller->writer,"void %s_destroy(struct %s* self);\n",c_name,c_name);
            //write class prototype functions
            for (int j=0; j<c_node->nptr_list.length; ++j) {
                _parser_node *f_node = LIST_AT_PTR(_parser_node,&c_node->nptr_list,j);
                //process function nodes
                if (_parser_is_type(&controller->parser,f_node,"function")) {
                    //write class function prototype
                    _controller_process(controller,f_node,_controller_c11_proc_deffun);
                    _file_putf(&controller->writer,";\n");
                }
            }
        }
    }
    return true;
}

int _controller_c11_proc_source(_controller *controller) {
    _parser_node *root = &controller->parser.tree;
    for (int i=0; i<root->nptr_list.length; ++i) {
        _parser_node *c_node = LIST_AT_PTR(_parser_node,&root->nptr_list,i);
        if (_parser_is_type(&controller->parser,c_node,"class")) {
            //fetch class attributes
            const char *c_name = _dict_at(&c_node->attr_dict,"name");
            //write class create functions
            //_file_putf(&controller->writer,"struct %s* %s_create() {\n",c_name,c_name);
            //_file_putf(&controller->writer,"\tstruct %s *self = malloc(sizeof(%s));\n",c_name,c_name);
            //_file_putf(&controller->writer,"\treturn self;\n}\n");
            //write class delete functions
            //_file_putf(&controller->writer,"void %s_destroy(struct %s* self) {\n",c_name,c_name);
            //_file_putf(&controller->writer,"}\n");
            //write class function implementations
            for (int j=0; j<c_node->nptr_list.length; ++j) {
                _parser_node *f_node = LIST_AT_PTR(_parser_node,&c_node->nptr_list,j);
                //process function nodes
                if (_parser_is_type(&controller->parser,f_node,"function")) {
                    //write class function declaration & body
                    _controller_process(controller,f_node,_controller_c11_proc_deffun);
                    _controller_explore(controller,f_node,_controller_c11_proc_defexp);
                    _controller_process(controller,f_node,_controller_c11_proc_deffgc);

                }
            }
        }
    }
    //write main function
    _parser_node *main = _parser_node_search_type(&controller->parser,root,"main");
    if (main != NULL) {
        _file_putf(&controller->writer,"int main(int argc, char *argv[])");
        _controller_explore(controller,main,_controller_c11_proc_defexp);
        _controller_process(controller,main,_controller_c11_proc_deffgc);
    }
    return true;
}

int _controller_c11_proc(_controller *controller) {
    //process syntax tree and generate c source file
    if (_file_open(&controller->writer,"out.c")) {
        //write header information
        char buffer[100];
        strftime(buffer,100,"%Y-%m-%d %H:%M:%S.000",localtime(&(time_t){time(0)}));
        _file_putf(&controller->writer,"// Generated by xc\n");
        _file_putf(&controller->writer,"// %s\n\n",buffer);
        _file_putf(&controller->writer,"#include <stdint.h>\n",buffer);
        //explore and process class headers
        _file_putf(&controller->writer,"\n// Definitions & Prototypes\n",buffer);
        _controller_c11_proc_header(controller);
        //explore and process class sources
        _file_putf(&controller->writer,"\n// Implementation\n\n",buffer);
        _controller_c11_proc_source(controller);
    }
    _file_destroy(&controller->writer);
    return true;
}
