#ifndef APP_API_H
#define APP_API_H

//! Libraries

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdarg.h>
#include <time.h>
#include <assert.h>
#include <math.h>
#include <string.h>
#include <unistd.h>

//! Utilities

void* _memdupl(const void *value, int size_e);
char* _strdupl(const char *value, int length);

typedef struct _list_t {
    void *data;
    int size_e, capacity, length;
} _list;

int _list_init(_list *list, int size_e);
int _list_reserve(_list *list, int capacity);
int _list_destroy(_list *list);
int _list_insert(_list *list, const void *data);
int _list_reverse(_list *list);
void* _list_data(_list *list ,int index);
void* _list_at(_list *list, int index);
void* _list_back(_list *list, int index);
int _list_remove(_list *list, int index);
int _list_wipe(_list *list);
int _list_is_empty(_list *list);

#define LIST_AT_PTR(e,l,i) (*(e**)_list_at(l,i))

typedef struct _pair_t {
    union { void *kp; int kv; };
    union { void *vp; int vv; };
    int size_k, size_v;
} _pair;

typedef struct _dict_t {
    _list pair_list;
} _dict;

int _dict_init(_dict *dict);
int _dict_destroy(_dict *dict);
int _dict_insert(_dict *dict, const char *name, const void *data, int size_e);
void* _dict_at(_dict *dict, const char *name);
void* _dict_at_index(_dict *dict, int index, int f_name);
int _dict_index(_dict *dict, const char *name);
int _dict_wipe(_dict *dict);
int _dict_is_empty(_dict *dict);

int _flag_has(int flags, int flag);
int _flag_set(int flags, int flag);

typedef struct _file_t {
    FILE *file;
    int scan_data;
    int scan_line;
    int mode;
} _file;

int _file_init(_file *file, const char *mode);
int _file_destroy(_file *file);
int _file_open(_file *file, const char *path);
int _file_scan(_file *file);
int _file_putf(_file *file, const char *text, ...);
int _file_is_ready(_file *file);

//! XC Parser

typedef struct _parser_token_t {
    _list char_list;
    int t_index, line;
} _parser_token;

int _parser_token_init(_parser_token *token);
int _parser_token_destroy(_parser_token *token);
int _parser_token_push_char(_parser_token *token, char character);
int _parser_token_reset(_parser_token *token);

typedef struct _parser_lexer_t {
    _list token_list;
    _dict token_type_dict;
} _parser_lexer;

int _parser_lexer_init(_parser_lexer *lexer);
int _parser_lexer_destroy(_parser_lexer *lexer);
int _parser_lexer_define(_parser_lexer *lexer, const char *code);
int _parser_lexer_push(_parser_lexer *lexer, _parser_token *token);
int _parser_lexer_read(_parser_lexer *lexer, _parser_token *token, char character);
int _parser_lexer_in_string(char character, const char *text);
int _parser_lexer_is_numeric(char character);
int _parser_lexer_is_alphanumeric(char data);
int _parser_lexer_is_symbol(char character);
int _parser_lexer_exec(_parser_lexer *lexer, _file *file);
int _parser_lexer_wipe(_parser_lexer *lexer);

typedef struct _parser_node_t {
    _dict attr_dict;
    _list nptr_list;
    struct _parser_node_t *parent;
    int r_index, n_ident;
    int t_index, t_start;
} _parser_node;

int _parser_node_init(_parser_node *node);
int _parser_node_destroy(_parser_node *node, int f_recursive);
int _parser_node_push_attr(_parser_node *node, const char *name, const char *data);
int _parser_node_push_node(_parser_node *node, _parser_node *data, int f_merge);

typedef struct _parser_stmt_t {
    const char *s_attr, *s_data;
    int s_value, s_flags;
} _parser_stmt;

typedef struct _parser_rule_t {
    _list data_list;
    _list stmt_list;
    int r_index, r_flags;
} _parser_rule;

int _parser_rule_init(_parser_rule *rule);
int _parser_rule_destroy(_parser_rule *rule);
int _parser_rule_push_stmt(_parser_rule *rule, _parser_stmt *stmt);
int _parser_rule_data_wipe(_parser_rule *rule);
_pair* _parser_rule_data_find(_parser_rule *rule, int t_start);
int _parser_rule_push_data(_parser_rule *rule, _pair *data);

typedef struct _parser_frame_t {
    _parser_node *curr, *prev;
    int r_index, s_index, o_flags;
} _parser_frame;

typedef struct _parser_t {
    _dict rule_dict;
    _parser_lexer lexer;
    _parser_node tree;
    int n_count;
} _parser;

int _parser_init(_parser *parser);
int _parser_destroy(_parser *parser);
int _parser_rule_define(_parser *parser, const char *name, int flags);
int _parser_rule_branch(_parser *parser, const char *name, const char *rule);
int _parser_rule_expect(_parser *parser, const char *name, const char *code);
int _parser_rule_attrib(_parser *parser, const char *name, const char *attr);
int _parser_rule_mfname(_parser *parser, const char *name, const char *attr);
int _parser_rule_mfdata(_parser *parser, const char *name, const char *data);
int _parser_rule_option(_parser *parser, const char *name, const char *rule);
int _parser_rule_repeat(_parser *parser, const char *name, const char *rule);
int _parser_rule_altern(_parser *parser, const char *name, const char *rule);
int _parser_read(_parser *parser, _parser_rule *rule);
_parser_node* _parser_node_malloc(_parser *parser, const char *type);
_parser_node* _parser_node_search_type(_parser *parser, _parser_node *node, const char *type);
_parser_node* _parser_node_search_attr(_parser *parser, _parser_node *node, const char *attr, const char *data);
int _parser_is_type(_parser *parser, _parser_node *node, const char *type);
int _parser_is_attr(_parser *parser, _parser_node *node, const char *attr);
int _parser_exec(_parser *parser, const char *path);

//! XC Controller

struct _controller_t;

typedef int (*_controller_fn_init)(struct _controller_t*);
typedef struct _controller_t* (*_controller_fn_factory)(void);
typedef int (*_controller_fn_destroy)(struct _controller_t*);
typedef int (*_controller_fn_grammar)(struct _controller_t*);
typedef int (*_controller_fn_proc)(struct _controller_t*);
typedef int (*_controller_fn_process)(struct _controller_t*,_list*,int);

typedef struct _controller_t {
    _parser parser;
    _file writer;
    _controller_fn_init init;
    _controller_fn_factory factory;
    _controller_fn_destroy destroy;
    _controller_fn_grammar grammar;
    _controller_fn_proc proc;
} _controller;

typedef struct _controller_factory_t {
    _controller_fn_factory fn;
} _controller_factory;

int _controller_init(_controller *controller, _dict *args);
_controller* _controller_malloc(void);
int _controller_destroy(_controller *controller);
int _controller_grammar(_controller *controller);
int _controller_process(_controller *controller, _parser_node *node, _controller_fn_process fn);
int _controller_explore(_controller *controller, _parser_node *node, _controller_fn_process fn);
int _controller_preproc(_controller *controller, _list *path, int face);
int _controller_exec(_controller *controller, _dict *args);

//! XC C11 Controller

int _controller_c11_init(_controller *controller);
_controller* _controller_c11_factory(void);
int _controller_c11_destroy(_controller *controller);
int _controller_c11_grammar(_controller *controller);
int _controller_c11_proc_deftab(_controller *controller, _list *path, int face);
int _controller_c11_proc_deftyp(_controller *controller, _list *path, int face);
int _controller_c11_proc_defvar(_controller *controller, _list *path, int face);
int _controller_c11_proc_defref(_controller *controller, _list *path, int face);
int _controller_c11_proc_defopi(_controller *controller, _list *path, int face);
int _controller_c11_proc_defopa(_controller *controller, _list *path, int face);
int _controller_c11_proc_defexp(_controller *controller, _list *path, int face);
int _controller_c11_proc_deffgc(_controller *controller, _list *path, int face);
int _controller_c11_proc_deffpl(_controller *controller, _list *path, int face);
int _controller_c11_proc_deffun(_controller *controller, _list *path, int face);
int _controller_c11_proc_header(_controller *controller);
int _controller_c11_proc_source(_controller *controller);
int _controller_c11_proc(_controller *controller);

//! XC XML Controller

int _controller_xml_init(_controller *controller);
_controller* _controller_xml_factory(void);
int _controller_xml_destroy(_controller *controller);
int _controller_xml_grammar(_controller *controller);
int _controller_xml_proc_output(_controller *controller, _list *path, int face);
int _controller_xml_proc(_controller *controller);

//! XC Core

typedef struct _core_t {
    _dict ctrl_factory;
} _core;

int _core_init(_core *core);
int _core_destroy(_core *core);
int _core_define(_core *core, const char *name, _controller_fn_factory fn);
int _core_exec(_core *core, _dict *args);

#endif //APP_API_H
