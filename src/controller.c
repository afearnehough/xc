#include "app_api.h"
#include "const.h"

//! Controller Functions

int _controller_init(_controller *controller, _dict *args) {
    //initialise file parser
    _parser_init(&controller->parser);
    //initialise file writer
    _file_init(&controller->writer,"write");
    //initialise language controller
    if (controller->init(controller)) {
        //initialise language grammar rules
        if (_controller_grammar(controller)) {
            if (controller->grammar(controller)) {
                return true;
            }
        }
    }
    return false;
}

_controller* _controller_malloc(void) {
    //create new controller object
    _controller *controller = malloc(sizeof(_controller));
    assert(controller != NULL);
    return controller;
}

int _controller_destroy(_controller *controller) {
    //clean-up resources
    _parser_destroy(&controller->parser);
    _file_destroy(&controller->writer);
    //clean-up language controller resources
    if (controller->destroy(controller)) {
        return true;
    }
    return false;
}

int _controller_grammar(_controller *controller) {
    _parser *parser = &controller->parser;
    //pre-define grammar rules
    _parser_rule_define(parser,"val",0);
    _parser_rule_define(parser,"ref",0);
    _parser_rule_define(parser,"str",0);
    _parser_rule_define(parser,"true",RULE_STICK);
    _parser_rule_define(parser,"false",RULE_STICK);
    _parser_rule_define(parser,"self",RULE_STICK);
    _parser_rule_define(parser,"null",RULE_STICK);
    _parser_rule_define(parser,"sub",0);
    _parser_rule_define(parser,"$expr0",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr1",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr2",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr3",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr4",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr5",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr6",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr7",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr8",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"$expr9",RULE_PASSIVE|RULE_CACHED);
    _parser_rule_define(parser,"expr",RULE_CACHED);
    _parser_rule_define(parser,"data_type",RULE_CACHED);
    _parser_rule_define(parser,"invoke",RULE_CACHED);
    _parser_rule_define(parser,"$array_decl",RULE_CACHED|RULE_PASSIVE);
    _parser_rule_define(parser,"array_size",RULE_CACHED);
    _parser_rule_define(parser,"condition",RULE_CACHED);
    _parser_rule_define(parser,"op_array",RULE_CACHED);
    _parser_rule_define(parser,"op_access",RULE_CACHED);
    _parser_rule_define(parser,"op_static",RULE_CACHED);
    _parser_rule_define(parser,"op_invoke",RULE_CACHED);
    _parser_rule_define(parser,"op_inc",RULE_CACHED);
    _parser_rule_define(parser,"op_dec",RULE_CACHED);
    _parser_rule_define(parser,"op_bind",RULE_CACHED);
    _parser_rule_define(parser,"op_pos",RULE_CACHED);
    _parser_rule_define(parser,"op_neg",RULE_CACHED);
    _parser_rule_define(parser,"op_not",RULE_CACHED);
    _parser_rule_define(parser,"op_cast",RULE_CACHED);
    _parser_rule_define(parser,"op_create",RULE_CACHED);
    _parser_rule_define(parser,"op_mul",RULE_CACHED);
    _parser_rule_define(parser,"op_div",RULE_CACHED);
    _parser_rule_define(parser,"op_mod",RULE_CACHED);
    _parser_rule_define(parser,"op_add",RULE_CACHED);
    _parser_rule_define(parser,"op_sub",RULE_CACHED);
    _parser_rule_define(parser,"op_gt",RULE_CACHED);
    _parser_rule_define(parser,"op_gte",RULE_CACHED);
    _parser_rule_define(parser,"op_lt",RULE_CACHED);
    _parser_rule_define(parser,"op_lte",RULE_CACHED);
    _parser_rule_define(parser,"op_equal",RULE_CACHED);
    _parser_rule_define(parser,"op_not_equal",RULE_CACHED);
    _parser_rule_define(parser,"op_and",RULE_CACHED);
    _parser_rule_define(parser,"op_or",RULE_CACHED);
    _parser_rule_define(parser,"op_assign",RULE_CACHED);
    _parser_rule_define(parser,"return",RULE_STICK);
    _parser_rule_define(parser,"break",RULE_STICK);
    _parser_rule_define(parser,"continue",RULE_STICK);
    _parser_rule_define(parser,"scope",RULE_CACHED|RULE_STICK);
    _parser_rule_define(parser,"if",0);
    _parser_rule_define(parser,"elseif",0);
    _parser_rule_define(parser,"else",0);
    _parser_rule_define(parser,"while",0);
    _parser_rule_define(parser,"for",0);
    _parser_rule_define(parser,"stmt",0);
    _parser_rule_define(parser,"template_type",0);
    _parser_rule_define(parser,"$template_head",RULE_PASSIVE);
    _parser_rule_define(parser,"$template_tail",RULE_PASSIVE);
    _parser_rule_define(parser,"template_impl",0);
    _parser_rule_define(parser,"template",0);
    _parser_rule_define(parser,"$variable_decl",RULE_PASSIVE);
    _parser_rule_define(parser,"variable_init",0);
    _parser_rule_define(parser,"variable",0);
    _parser_rule_define(parser,"argument",0);
    _parser_rule_define(parser,"$argument_head",RULE_PASSIVE);
    _parser_rule_define(parser,"$argument_tail",RULE_PASSIVE);
    _parser_rule_define(parser,"argument_list",0);
    _parser_rule_define(parser,"parameter",0);
    _parser_rule_define(parser,"$parameter_head",RULE_PASSIVE);
    _parser_rule_define(parser,"$parameter_tail",RULE_PASSIVE);
    _parser_rule_define(parser,"parameter_list",0);
    _parser_rule_define(parser,"create",0);
    _parser_rule_define(parser,"delete",0);
    _parser_rule_define(parser,"$function_type",RULE_PASSIVE);
    _parser_rule_define(parser,"$function_stmt",RULE_PASSIVE);
    _parser_rule_define(parser,"function",0);
    _parser_rule_define(parser,"$class_stmt",RULE_PASSIVE);
    _parser_rule_define(parser,"class_base",0);
    _parser_rule_define(parser,"class",0);
    _parser_rule_define(parser,"main",0);
    _parser_rule_define(parser,"using",0);
    _parser_rule_define(parser,"$root_stmt",RULE_PASSIVE);
    _parser_rule_define(parser,"root",0);
    //define rule statements: val
    _parser_rule_expect(parser,"val","<number>");
    _parser_rule_attrib(parser,"val","value");
    //define rule statements: ref
    _parser_rule_expect(parser,"ref","<identity>");
    _parser_rule_attrib(parser,"ref","name");
    //define rule statements: str
    _parser_rule_expect(parser,"str","<string>");
    _parser_rule_attrib(parser,"str","value");
    //define rule statements: true
    _parser_rule_expect(parser,"true","true");
    //define rule statements: false
    _parser_rule_expect(parser,"false","false");
    //define rule statements: self
    _parser_rule_expect(parser,"self","self");
    //define rule statements: null
    _parser_rule_expect(parser,"null","null");
    //define rule statements: sub
    _parser_rule_expect(parser,"sub","(");
    _parser_rule_branch(parser,"sub","expr");
    _parser_rule_expect(parser,"sub",")");
    //define rule statements: $expr0
    _parser_rule_branch(parser,"$expr0","val");
    _parser_rule_altern(parser,"$expr0","ref");
    _parser_rule_altern(parser,"$expr0","str");
    _parser_rule_altern(parser,"$expr0","true");
    _parser_rule_altern(parser,"$expr0","false");
    _parser_rule_altern(parser,"$expr0","self");
    _parser_rule_altern(parser,"$expr0","null");
    _parser_rule_altern(parser,"$expr0","sub");
    //define rule statements: $expr1
    _parser_rule_branch(parser,"$expr1","op_array");
    _parser_rule_altern(parser,"$expr1","op_access");
    _parser_rule_altern(parser,"$expr1","op_static");
    _parser_rule_altern(parser,"$expr1","op_invoke");
    _parser_rule_altern(parser,"$expr1","op_inc");
    _parser_rule_altern(parser,"$expr1","op_dec");
    _parser_rule_altern(parser,"$expr1","op_bind");
    _parser_rule_altern(parser,"$expr1","$expr0");
    //define rule statements: $expr2
    _parser_rule_branch(parser,"$expr2","op_pos");
    _parser_rule_altern(parser,"$expr2","op_neg");
    _parser_rule_altern(parser,"$expr2","op_not");
    _parser_rule_altern(parser,"$expr2","$expr1");
    //define rule statements: $expr3
    _parser_rule_branch(parser,"$expr3","op_create");
    _parser_rule_altern(parser,"$expr3","$expr2");
    //define rule statements: $expr4
    _parser_rule_branch(parser,"$expr4","op_mul");
    _parser_rule_altern(parser,"$expr4","op_div");
    _parser_rule_altern(parser,"$expr4","op_mod");
    _parser_rule_altern(parser,"$expr4","$expr3");
    //define rule statements: $expr5
    _parser_rule_branch(parser,"$expr5","op_add");
    _parser_rule_altern(parser,"$expr5","op_sub");
    _parser_rule_altern(parser,"$expr5","$expr4");
    //define rule statements: $expr6
    _parser_rule_branch(parser,"$expr6","op_gt");
    _parser_rule_altern(parser,"$expr6","op_gte");
    _parser_rule_altern(parser,"$expr6","op_lt");
    _parser_rule_altern(parser,"$expr6","op_lte");
    _parser_rule_altern(parser,"$expr6","$expr5");
    //define rule statements: $expr7
    _parser_rule_branch(parser,"$expr7","op_equal");
    _parser_rule_altern(parser,"$expr7","op_not_equal");
    _parser_rule_altern(parser,"$expr7","$expr6");
    //define rule statements: $expr8
    _parser_rule_branch(parser,"$expr8","op_and");
    _parser_rule_altern(parser,"$expr8","$expr7");
    //define rule statements: $expr9
    _parser_rule_branch(parser,"$expr9","op_or");
    _parser_rule_altern(parser,"$expr9","$expr8");
    //define rule statements: expression
    _parser_rule_branch(parser,"expr","op_assign");
    _parser_rule_altern(parser,"expr","$expr9");
    //define rule statements: data_type
    _parser_rule_expect(parser,"data_type","<identity>");
    _parser_rule_attrib(parser,"data_type","type");
    _parser_rule_option(parser,"data_type","template");
    //define rule statements: invoke
    _parser_rule_expect(parser,"invoke","(");
    _parser_rule_option(parser,"invoke","argument_list");
    _parser_rule_expect(parser,"invoke",")");
    //define rule statements: array_decl
    _parser_rule_expect(parser,"$array_decl","[");
    _parser_rule_expect(parser,"$array_decl","]");
    _parser_rule_mfname(parser,"$array_decl","array");
    _parser_rule_mfdata(parser,"$array_decl","true");
    //define rule statements: array_size
    _parser_rule_expect(parser,"array_size","[");
    _parser_rule_branch(parser,"array_size","expr");
    _parser_rule_expect(parser,"array_size","]");
    //define rule statements: conditionition
    _parser_rule_expect(parser,"condition","(");
    _parser_rule_branch(parser,"condition","expr");
    _parser_rule_expect(parser,"condition",")");
    //define rule statements: array access operation
    _parser_rule_branch(parser,"op_array","$expr0");
    _parser_rule_expect(parser,"op_array","[");
    _parser_rule_branch(parser,"op_array","expr");
    _parser_rule_expect(parser,"op_array","]");
    //define rule statements: access operation
    _parser_rule_branch(parser,"op_access","$expr0");
    _parser_rule_expect(parser,"op_access",".");
    _parser_rule_branch(parser,"op_access","$expr1");
    //define rule statements: static access operation
    _parser_rule_branch(parser,"op_static","ref");
    _parser_rule_expect(parser,"op_static","~");
    _parser_rule_branch(parser,"op_static","$expr1");
    //define rule statements: invoke operation
    _parser_rule_branch(parser,"op_invoke","$expr0");
    _parser_rule_expect(parser,"op_invoke","(");
    _parser_rule_option(parser,"op_invoke","argument_list");
    _parser_rule_expect(parser,"op_invoke",")");
    //define rule statements: increment operation
    _parser_rule_branch(parser,"op_inc","$expr0");
    _parser_rule_expect(parser,"op_inc","++");
    //define rule statements: decrement operation
    _parser_rule_branch(parser,"op_dec","$expr0");
    _parser_rule_expect(parser,"op_dec","--");
    //define rule statements: bind operation
    _parser_rule_expect(parser,"op_bind","@");
    _parser_rule_branch(parser,"op_bind","op_invoke");
    _parser_rule_altern(parser,"op_bind","$expr0");
    //define rule statements: positive operation
    _parser_rule_expect(parser,"op_pos","+");
    _parser_rule_branch(parser,"op_pos","$expr1");
    //define rule statements: negative operation
    _parser_rule_expect(parser,"op_neg","-");
    _parser_rule_branch(parser,"op_neg","$expr1");
    //define rule statements: conditionitional NOT operation
    _parser_rule_expect(parser,"op_not","!");
    _parser_rule_branch(parser,"op_not","$expr1");
    //define rule statements: cast operation
    //define rule statements: create operation
    _parser_rule_expect(parser,"op_create","create");
    _parser_rule_branch(parser,"op_create","data_type");
    _parser_rule_branch(parser,"op_create","invoke");
    _parser_rule_altern(parser,"op_create","array_size");
    //define rule statements: multiplication operation
    _parser_rule_branch(parser,"op_mul","$expr3");
    _parser_rule_expect(parser,"op_mul","*");
    _parser_rule_branch(parser,"op_mul","$expr4");
    //define rule statements: division operation
    _parser_rule_branch(parser,"op_div","$expr3");
    _parser_rule_expect(parser,"op_div","/");
    _parser_rule_branch(parser,"op_div","$expr4");
    //define rule statements: modulus operation
    _parser_rule_branch(parser,"op_mod","$expr3");
    _parser_rule_expect(parser,"op_mod","%");
    _parser_rule_branch(parser,"op_mod","$expr4");
    //define rule statements: addition operation
    _parser_rule_branch(parser,"op_add","$expr4");
    _parser_rule_expect(parser,"op_add","+");
    _parser_rule_branch(parser,"op_add","$expr5");
    //define rule statements: subtraction operation
    _parser_rule_branch(parser,"op_sub","$expr4");
    _parser_rule_expect(parser,"op_sub","-");
    _parser_rule_branch(parser,"op_sub","$expr5");
    //define rule statements: conditional GREATER THAN operation
    _parser_rule_branch(parser,"op_gt","$expr5");
    _parser_rule_expect(parser,"op_gt",">");
    _parser_rule_branch(parser,"op_gt","$expr6");
    //define rule statements: conditional GREATER THAN OR EQUAL TO operation
    _parser_rule_branch(parser,"op_gte","$expr5");
    _parser_rule_expect(parser,"op_gte",">=");
    _parser_rule_branch(parser,"op_gte","$expr6");
    //define rule statements: conditional LESS THAN operation
    _parser_rule_branch(parser,"op_lt","$expr5");
    _parser_rule_expect(parser,"op_lt","<");
    _parser_rule_branch(parser,"op_lt","$expr6");
    //define rule statements: conditional LESS THAN OR EQUAL TO operation
    _parser_rule_branch(parser,"op_lte","$expr5");
    _parser_rule_expect(parser,"op_lte","<=");
    _parser_rule_branch(parser,"op_lte","$expr6");
    //define rule statements: conditional EQUAL TO operation
    _parser_rule_branch(parser,"op_equal","$expr6");
    _parser_rule_expect(parser,"op_equal","==");
    _parser_rule_branch(parser,"op_equal","$expr7");
    //define rule statements: conditional NOT EQUAL TO operation
    _parser_rule_branch(parser,"op_not_equal","$expr6");
    _parser_rule_expect(parser,"op_not_equal","!=");
    _parser_rule_branch(parser,"op_not_equal","$expr7");
    //define rule statements: conditional AND operation
    _parser_rule_branch(parser,"op_and","$expr7");
    _parser_rule_expect(parser,"op_and","and");
    _parser_rule_branch(parser,"op_and","$expr8");
    //define rule statements: conditional OR operation
    _parser_rule_branch(parser,"op_or","$expr8");
    _parser_rule_expect(parser,"op_or","or");
    _parser_rule_branch(parser,"op_or","$expr9");
    //define rule statements: assign operation
    _parser_rule_branch(parser,"op_assign","$expr1");
    _parser_rule_expect(parser,"op_assign","=");
    _parser_rule_branch(parser,"op_assign","$expr9");
    //define rule statements: return statement
    _parser_rule_expect(parser,"return","return");
    _parser_rule_option(parser,"return","expr");
    _parser_rule_expect(parser,"return",";");
    //define rule statements: break statement
    _parser_rule_expect(parser,"break","break");
    _parser_rule_expect(parser,"break",";");
    //define rule statements: continue statement
    _parser_rule_expect(parser,"continue","continue");
    _parser_rule_expect(parser,"continue",";");
    //define rule statements: scope block
    _parser_rule_expect(parser,"scope","{");
    _parser_rule_repeat(parser,"scope","$function_stmt");
    _parser_rule_expect(parser,"scope","}");
    //define rule statements: if statement
    _parser_rule_expect(parser,"if","if");
    _parser_rule_branch(parser,"if","condition");
    _parser_rule_branch(parser,"if","scope");
    _parser_rule_repeat(parser,"if","elseif");
    _parser_rule_option(parser,"if","else");
    //define rule statements: else if statement
    _parser_rule_expect(parser,"elseif","else");
    _parser_rule_expect(parser,"elseif","if");
    _parser_rule_branch(parser,"elseif","condition");
    _parser_rule_branch(parser,"elseif","scope");
    //define rule statements: else statement
    _parser_rule_expect(parser,"else","else");
    _parser_rule_branch(parser,"else","scope");
    //define rule statements: while statement
    _parser_rule_expect(parser,"while","while");
    _parser_rule_branch(parser,"while","condition");
    _parser_rule_branch(parser,"while","scope");
    //define rule statements: for statement
    _parser_rule_expect(parser,"for","for");
    _parser_rule_expect(parser,"for","(");
    _parser_rule_option(parser,"for","expr");
    _parser_rule_expect(parser,"for",";");
    _parser_rule_option(parser,"for","expr");
    _parser_rule_expect(parser,"for",";");
    _parser_rule_option(parser,"for","expr");
    _parser_rule_expect(parser,"for",")");
    _parser_rule_branch(parser,"for","scope");
    //define rule statements: function expression statement
    _parser_rule_branch(parser,"stmt","expr");
    _parser_rule_expect(parser,"stmt",";");
    //define rule statements: template types/list
    _parser_rule_branch(parser,"template_type","data_type");
    _parser_rule_branch(parser,"$template_head","template_type");
    _parser_rule_expect(parser,"$template_tail",",");
    _parser_rule_branch(parser,"$template_tail","$template_head");
    _parser_rule_expect(parser,"template","<");
    _parser_rule_branch(parser,"template","$template_head");
    _parser_rule_repeat(parser,"template","$template_tail");
    _parser_rule_expect(parser,"template",">");
    //define rule statements: variable declaration
    _parser_rule_expect(parser,"$variable_decl","<identity>");
    _parser_rule_attrib(parser,"$variable_decl","name");
    _parser_rule_expect(parser,"$variable_decl",":");
    _parser_rule_branch(parser,"$variable_decl","data_type");
    _parser_rule_option(parser,"$variable_decl","$array_decl");
    //define rule statements: variable value initialisation
    _parser_rule_expect(parser,"variable_init","=");
    _parser_rule_branch(parser,"variable_init","expr");
    //define rule statements: variable
    _parser_rule_expect(parser,"variable","var");
    _parser_rule_branch(parser,"variable","$variable_decl");
    _parser_rule_option(parser,"variable","variable_init");
    _parser_rule_expect(parser,"variable",";");
    //define rule statements: argument
    _parser_rule_branch(parser,"argument","expr");
    //define rule statements: argument list head/tail
    _parser_rule_branch(parser,"$argument_head","argument");
    _parser_rule_expect(parser,"$argument_tail",",");
    _parser_rule_branch(parser,"$argument_tail","$argument_head");
    //define rule statements: argument list
    _parser_rule_branch(parser,"argument_list","$argument_head");
    _parser_rule_repeat(parser,"argument_list","$argument_tail");
    //define rule statements: parameter
    _parser_rule_branch(parser,"parameter","$variable_decl");
    //define rule statements: parameter list head/tail
    _parser_rule_branch(parser,"$parameter_head","parameter");
    _parser_rule_expect(parser,"$parameter_tail",",");
    _parser_rule_branch(parser,"$parameter_tail","$parameter_head");
    //define rule statements: parameter list
    _parser_rule_branch(parser,"parameter_list","$parameter_head");
    _parser_rule_repeat(parser,"parameter_list","$parameter_tail");
    //define rule statements: create
    _parser_rule_expect(parser,"create","create");
    _parser_rule_expect(parser,"create","(");
    _parser_rule_option(parser,"create","parameter_list");
    _parser_rule_expect(parser,"create",")");
    _parser_rule_branch(parser,"create","scope");
    //define rule statements: delete
    _parser_rule_expect(parser,"delete","delete");
    _parser_rule_expect(parser,"delete","(");
    _parser_rule_option(parser,"delete","parameter_list");
    _parser_rule_expect(parser,"delete",")");
    _parser_rule_branch(parser,"delete","scope");
    //define rule statements: function type
    _parser_rule_expect(parser,"$function_type",":");
    _parser_rule_branch(parser,"$function_type","data_type");
    //define rule statements: function statement
    _parser_rule_branch(parser,"$function_stmt","variable");
    _parser_rule_altern(parser,"$function_stmt","if");
    _parser_rule_altern(parser,"$function_stmt","while");
    _parser_rule_altern(parser,"$function_stmt","for");
    _parser_rule_altern(parser,"$function_stmt","scope");
    _parser_rule_altern(parser,"$function_stmt","break");
    _parser_rule_altern(parser,"$function_stmt","continue");
    _parser_rule_altern(parser,"$function_stmt","return");
    _parser_rule_altern(parser,"$function_stmt","stmt");
    //define rule statements: function
    _parser_rule_expect(parser,"function","fn");
    _parser_rule_expect(parser,"function","<identity>");
    _parser_rule_attrib(parser,"function","name");
    _parser_rule_expect(parser,"function","(");
    _parser_rule_option(parser,"function","parameter_list");
    _parser_rule_expect(parser,"function",")");
    _parser_rule_option(parser,"function","$function_type");
    _parser_rule_branch(parser,"function","scope");
    //define rule statements: class statement
    _parser_rule_branch(parser,"$class_stmt","variable");
    _parser_rule_altern(parser,"$class_stmt","create");
    _parser_rule_altern(parser,"$class_stmt","delete");
    _parser_rule_altern(parser,"$class_stmt","function");
    //define rule statements: class inheritance
    _parser_rule_expect(parser,"class_base","extends");
    _parser_rule_branch(parser,"class_base","data_type");
    //define rule statements: class
    _parser_rule_expect(parser,"class","class");
    _parser_rule_expect(parser,"class","<identity>");
    _parser_rule_attrib(parser,"class","name");
    _parser_rule_option(parser,"class","template");
    _parser_rule_option(parser,"class","class_base");
    _parser_rule_expect(parser,"class","{");
    _parser_rule_repeat(parser,"class","$class_stmt");
    _parser_rule_expect(parser,"class","}");
    //define rule statements: main function
    _parser_rule_expect(parser,"main","main");
    _parser_rule_expect(parser,"main","(");
    _parser_rule_option(parser,"main","parameter_list");
    _parser_rule_expect(parser,"main",")");
    _parser_rule_branch(parser,"main","scope");
    //define rule statements: using statement
    _parser_rule_expect(parser,"using","using");
    _parser_rule_expect(parser,"using","<string>");
    _parser_rule_attrib(parser,"using","path");
    _parser_rule_expect(parser,"using",";");
    //define rule statements: root statement
    _parser_rule_branch(parser,"$root_stmt","using");
    _parser_rule_altern(parser,"$root_stmt","main");
    _parser_rule_altern(parser,"$root_stmt","class");
    //define rule statements: root
    _parser_rule_repeat(parser,"root","$root_stmt");
    return true;
}

int _controller_process(_controller *controller, _parser_node *node, _controller_fn_process fn) {
    //create and initialise path stack
    _list path;
    _list_init(&path,sizeof(_pair));
    //insert node and all parent nodes into path list
    _parser_node *curr = node;
    while (curr != NULL) {
        _list_insert(&path,&(_pair) {
            .kp = curr, .vv = 0
        });
        curr = curr->parent;
    }
    //call specified process function
    int status = false;
    if (path.length) {
        _list_reverse(&path);
        status = fn(controller,&path,NODE_PROCESS_HEAD);
    }
    //clean-up path stack
    _list_destroy(&path);
    return status;
}

int _controller_explore(_controller *controller, _parser_node *node, _controller_fn_process fn) {
    //create and initialise path stack
    _list path;
    _list_init(&path,sizeof(_pair));
    _list_insert(&path,&(_pair) {
        .kp = node, .vv = 0
    });
    //parse all nodes until path stack is depleted
    while (path.length) {
        //fetch current node
        _pair nref = *(_pair*)_list_back(&path,0);
        _parser_node *node = nref.kp;
        //find current node face
        int n_face = NODE_PROCESS_HEAD;
        if (nref.vv == node->nptr_list.length) {
            n_face = NODE_PROCESS_LAST;
        } else if (nref.vv > 0) {
            n_face = NODE_PROCESS_MIDDLE;
        }
        //call specified process function
        int status = fn(controller,&path,n_face);
        //pop current node reference
        _list_remove(&path,path.length-1);
        //check for child node
        if (nref.vv < node->nptr_list.length) {
            _parser_node *curr = LIST_AT_PTR(_parser_node,&node->nptr_list,nref.vv);
            //push current current node reference
            _list_insert(&path,&(_pair) {
                .kp = node, .vv = nref.vv+1
            });
            if (status == NODE_EXPLORE_DEEPER) {
                //push node reference for next iteration
                _list_insert(&path,&(_pair) {
                    .kp = curr, .vv = 0
                });
            }
        }
    }
    //clean-up path stack
    _list_destroy(&path);
    return true;
}

int _controller_preproc(_controller *controller, _list *path, int face) {
    //fetch required node references
    _parser_node *node = ((_pair*)_list_back(path,0))->kp;
    assert(node != NULL);
    _parser_node *root = &controller->parser.tree;
    //handle node: variable
    if (_parser_is_type(&controller->parser,node,"variable")) {
        if (face == NODE_PROCESS_HEAD) {
            //fetch reference to data type node
            _parser_node *d_node = _parser_node_search_type(&controller->parser,node,"data_type");
            assert(d_node != NULL);
            //fetch data type attributes
            const char *d_name = _dict_at(&d_node->attr_dict,"type");
            //check if data type is not a template type
            //fetch reference to data type class node
            _parser_node *c_node = _parser_node_search_attr(&controller->parser,root,"name",d_name);
            if (c_node != NULL) {
                //check if class is templated
                //  check if class doesn't have template_impl node matching d_node template
                //    add new template_impl node with list of template_type's to class node
            } else {
                printf("Error: Type '%s' is not valid.\n",d_name);
            }
            /*t_node = _parser_node_search_type(&controller->parser,t_node,"template");
            if (t_node != NULL) {
                for (int i=0; i<t_node->nptr_list.length; ++i) {
                    _parser_node *curr = LIST_AT_PTR(_parser_node,&t_node->nptr_list,i);
                    _parser_node *curr = _parser_node_search_type(&controller->parser,t_node,"data_type");
                }
            }*/
        }
        return NODE_EXPLORE_IGNORE;
    }
    return NODE_EXPLORE_DEEPER;
}

int _controller_exec(_controller *controller,  _dict *args) {
    //check input file specified
    if (_dict_at(args,"i") != NULL) {
        _parser_node *root = &controller->parser.tree;
        _dict processed;
        _dict_init(&processed);
        int status = true;
        //get specified source file path
        char base_path[256];
        memset(&base_path[0],0,sizeof(base_path));
        char curr_file[256];
        memset(&curr_file[0],0,sizeof(curr_file));
        strcpy(curr_file,_dict_at(args,"i"));
        //find index of last path seperator
        for (int i=strlen(curr_file); i>0; --i) {
            if (curr_file[i] == '/') {
                strncpy(base_path,curr_file,i+1);
                base_path[i+1] = '\0';
                break;
            }
        }
        //process specified source file and dependent source files
        int reading = true;
        while (reading) {
            if ((status = _parser_exec(&controller->parser,curr_file))) {
                //add source file to processed list
                _dict_insert(&processed,curr_file,curr_file,strlen(curr_file)+1);
                reading = false;
                //resolve 'using' statements
                for (int i=0; i<root->nptr_list.length; ++i) {
                    _parser_node *node = LIST_AT_PTR(_parser_node,&root->nptr_list,i);
                    if (_parser_is_type(&controller->parser,node,"using")) {
                        //concatenate base path and 'using' node path
                        char buffer[512];
                        strcpy(buffer,base_path);
                        strcat(buffer,_dict_at(&node->attr_dict,"path"));
                        //check if file has not been processed alread
                        if (!_dict_at(&processed,buffer)) {
                            strcpy(curr_file,buffer);
                            reading = true;
                            break;
                        } else {
                            _list_remove(&root->nptr_list,i);
                        }
                    }
                }
            } else {
                reading = false;
            }
        }
        //clean-up processed files list
        //_dict_destroy(&processed);
        if (status) {
            //perform pre-processing
            _controller_explore(controller,root,_controller_preproc);
            //perform language controller processing
            if (controller->proc(controller)) {
                return true;
            }
        } else {
            printf("Error: Failed to parse file: %s\n",curr_file);
        }
    } else {
        printf("Error: No input file specified\n");
    }
    return false;
}
