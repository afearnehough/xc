#include "app_api.h"
#include "const.h"

//! XML Controller Functions

int _controller_xml_init(_controller *controller) {
    return true;
}

_controller* _controller_xml_factory(void) {
    //create new xml controller object
    _controller *controller = _controller_malloc();
    //attach controller functions
    controller->init = _controller_xml_init;
    controller->factory = _controller_xml_factory;
    controller->grammar = _controller_xml_grammar;
    controller->destroy = _controller_xml_destroy;
    controller->proc = _controller_xml_proc;
    return controller;
}

int _controller_xml_destroy(_controller *controller) {
    return true;
}

int _controller_xml_grammar(_controller *controller) {
    _parser *parser = &controller->parser;
    //TODO: define XML specific grammar here
    return true;
}

int _controller_xml_proc_output(_controller *controller, _list *path, int face) {
    //get node references
    _pair *nref = _list_back(path,0);
    _parser_node *curr = nref->kp;
    //get node attributes
    const char* name = _dict_at_index(&controller->parser.rule_dict,curr->r_index,true);
    //check if at head node
    if (nref->vv == 0) {
        for (int d=0; d<path->length-1; ++d) {
            _file_putf(&controller->writer,"\t");
        }
        _file_putf(&controller->writer,"<%s",name);
        for (int a=0; a<curr->attr_dict.pair_list.length; ++a) {
            _pair *pair = _list_at(&curr->attr_dict.pair_list,a);
            _file_putf(&controller->writer," %s=\"%s\"",pair->kp,pair->vp);
        }
        _file_putf(&controller->writer,">");
        if (curr->nptr_list.length > 0) {
            _file_putf(&controller->writer,"\n");
        }
    }
    //check if at last node
    if (nref->vv == curr->nptr_list.length) {
        if (curr->nptr_list.length > 0) {
            for (int d=0; d<path->length-1; ++d) {
                _file_putf(&controller->writer,"\t");
            }
        }
        _file_putf(&controller->writer,"</%s>\n",name);
    }
    return NODE_EXPLORE_DEEPER;
}

int _controller_xml_proc(_controller *controller) {
    if (_file_open(&controller->writer,"out.xml")) {
        _file_putf(&controller->writer,"<?xml version=\"1.0\"?>\n");
        //explore syntax tree and generate XML document
        _controller_explore(controller,&controller->parser.tree,_controller_xml_proc_output);
    }
    _file_destroy(&controller->writer);
    return true;
}
